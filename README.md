# an example client to see how gitlab-ci and keycloak-js works

important note: to sync newly built files with ibm cos bucket, you need to create a service credential for bucket with `Manager` role (sync need both read and write rights!) and HMAC credentials. HMAC credentials should be passed to gitlab ci/cd env variables. otherwise, you will get access error!
