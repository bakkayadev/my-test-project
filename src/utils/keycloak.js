import Keycloak from 'keycloak-js';

const kcConfig = {
  url: 'https://dev-keycloak.devtokeninc.com/auth/',
  realm: 'token_identity_realm',
  clientId: 'token-test-client',
};

const keycloak = new Keycloak(kcConfig);

export default keycloak;
