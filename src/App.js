import React from 'react';
import { ReactKeycloakProvider } from '@react-keycloak/web';

import keycloak from './utils/keycloak';

import Menu from './components/Menu';
import Routes from './routes';

function App() {
  React.useEffect(() => {
    console.log(keycloak);
  }, []);

  return (
    <ReactKeycloakProvider
      authClient={keycloak}
      initOptions={{
        onLoad: 'login-required',
      }}
    >
      <div className='App'>
        <Menu />
        <Routes />
      </div>
    </ReactKeycloakProvider>
  );
}

export default App;
