import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { useKeycloak } from '@react-keycloak/web';

import SecuredRoute from '../components/SecuredRoute';

import Home from '../components/Home';
import AdminPage from '../components/AdminPage';

const Routes = () => {
  const { initialized } = useKeycloak();

  if (!initialized) return <p>Loading...</p>;
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Home} />
        <SecuredRoute
          exact
          path='/admin'
          roles={['admin']}
          component={AdminPage}
        />
        <Route path='*'>
          <h1>not found.</h1>
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
