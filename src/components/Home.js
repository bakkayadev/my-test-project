import React from 'react';

import AuthorizedElement from './AuthorizedElement';

const Home = () => {
  return (
    <div>
      Dummy Home page - it's newly updated!
      <AuthorizedElement roles={['admin']}>
        <button>This button can only seen by an admin</button>
      </AuthorizedElement>
    </div>
  );
};

export default Home;
