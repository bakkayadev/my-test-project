import React from 'react';
import { useKeycloak } from '@react-keycloak/web';

import isAuthorized from '../utils/isAuthorized';

export default function AuthorizedElement({ roles, children }) {
  const { keycloak } = useKeycloak();
  return isAuthorized(keycloak, roles) && <>{children}</>;
}
