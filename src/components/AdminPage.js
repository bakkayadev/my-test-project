import React from 'react';

const AdminPage = () => {
  return <div>Dummy Admin page - only admin role users can see that!</div>;
};

export default AdminPage;
