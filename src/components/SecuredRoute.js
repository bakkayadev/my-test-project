import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useKeycloak } from '@react-keycloak/web';

import isAuthorized from '../utils/isAuthorized';

export default function SecuredRoute({ component: Component, roles, ...rest }) {
  const { keycloak } = useKeycloak();

  return (
    <Route
      {...rest}
      render={(props) => {
        if (!keycloak.authenticated) keycloak.login();
        return isAuthorized(keycloak, roles) ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/' }} />
        );
      }}
    />
  );
}
