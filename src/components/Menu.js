import { useKeycloak } from '@react-keycloak/web';
import React from 'react';
import isAuthorized from '../utils/isAuthorized';

const Menu = () => {
  const { keycloak } = useKeycloak();

  return (
    <ul>
      <li>
        <a href='/'>Home Page </a>
      </li>

      {isAuthorized(keycloak, ['admin']) && (
        <li>
          <a href='/admin'>Admin Page</a>
        </li>
      )}

      {keycloak && !keycloak.authenticated && (
        <li>
          <a className='btn-link' onClick={() => keycloak.login()} href='/#'>
            Login
          </a>
        </li>
      )}

      {keycloak && keycloak.authenticated && (
        <li>
          <a className='btn-link' onClick={() => keycloak.logout()} href='/#'>
            Logout ({keycloak.tokenParsed.preferred_username})
          </a>
        </li>
      )}
    </ul>
  );
};

export default Menu;
